var translations = [
  {
    value: 'Name is required',
    label: 'Podaj imię.'
  },
  {
    value: 'Name must be between 2 and 50 characters',
    label: 'Imię musi zawierać od 2 do 50 znaków.'
  },
  {
    value: 'Surname is required',
    label: 'Podaj nazwisko.'
  },
  {
    value: 'Surname must be between 2 and 50 characters',
    label: 'Nazwisko musi zawierać od 2 do 50 znaków.'
  },
  {
    value: 'Address is required',
    label: 'Podaj adres.'
  },
  {
    value: 'Address must be between 2 and 50 characters',
    label: 'Adres musi zawierać od 2 do 50 znaków.'
  },
  {
    value: 'Pcode is required',
    label: 'Podaj kod pocztowy.'
  },
  {
    value: 'Pcode must be numeric',
    label: 'Kod pocztowy musi być liczbą.'
  },
  {
    value: 'City is required',
    label: 'Podaj miejscowość.'
  },
  {
    value: 'City must be between 2 and 50 characters',
    label: 'Miejscowość musi zawierać od 2 do 50 znaków.'
  },
  {
    value: 'Phone is required',
    label: 'Podaj telefon kontaktowy.'
  },
  {
    value: 'Phone must be numeric',
    label: 'Telefon kontaktowy musi być liczbą.'
  },
  {
    value: 'Email is required',
    label: 'Podaj email.'
  },
  {
    value: 'Email must be between 5 and 50 characters',
    label: 'Email musi zawierać od 5 do 50 znaków.'
  },
  {
    value: 'Email is not a valid email address',
    label: 'Email jest nieprawidłowy.'
  },
];

Object.keys(errors).forEach(function (error) {
  var input = document.querySelector('input#' + error);
  var parentInput = input.parentElement;

  if (parentInput.className === 'city-wrapper') {
    parentInput = parentInput.parentElement;
  }

  var newDiv = document.createElement('div');
  newDiv.classList.add('errors');

  translations.find(function (trans) {
    errors[error].forEach(function (error) {
      if (trans.value === error) {
        var newP = document.createElement('p');
        newP.innerHTML = trans.label;
        newDiv.appendChild(newP);
      }
    })
  });

  parentInput.appendChild(newDiv);
});