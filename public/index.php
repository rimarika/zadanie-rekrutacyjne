<?php

$settings = require __DIR__ . '/../src/settings.php';

require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App($settings);

//set dependencies
require __DIR__ . '/../src/dependencies.php';

//set routes
require __DIR__ . '/../src/routes.php';

$app->run();
