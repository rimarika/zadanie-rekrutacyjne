<?php

namespace App\Controllers;

class BaseController
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function get($property)
    {
        if ($this->container->{$property}) {

            return $this->container->get($property);
        }
    }
}
