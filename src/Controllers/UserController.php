<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;

class UserController extends BaseController
{

  public function userAction(Request $request, Response $response, array $args)
  {
    if (!empty($args) && $args['id']) {
      $user = User::getUser($args['id']);

      return $this->get('view')->render($response, "userAdded.php", [
        'title' => 'Dane użytkownika',
        'user' => $user['data']
      ]);
    }
  }

  public function addAction(Request $request, Response $response)
  {
    $validator = $this->get('UserValidation');

    if ($request->isPost() && $validator->validate($request)) {

      $user = new User();

      $user = $user->setUser($request->getParams());

      return $response->withRedirect($this->get('router')->pathFor('usersAction', ['id' => $user->id]));
    }

    $errors = $validator->errors();

    return $this->get('view')->render($response, "user.php", [
      'title' => 'Formularz',
      'params' => $request->getParams(),
      'errors' => json_encode($errors)
    ]);
  }
}
