<?php

namespace App\Validation;

class UserValidation extends BaseValidation
{
    public function validate($request)
    {
        $this->rules = [
            'name' => ['required', ['lengthBetween', 2, 50]],
            'surname' => ['required', ['lengthBetween', 2, 50]],
            'address' => ['required', ['lengthBetween', 2, 50]],
            'pcode' => ['required', 'numeric'],
            'city' => ['required', ['lengthBetween', 2, 50]],
            'phone' => ['required', 'numeric'],
            'email' => ['required', ['lengthBetween', 5, 50], 'email']
        ];

        return parent::validate($request);
    }
}
