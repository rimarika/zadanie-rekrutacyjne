<?php

namespace App\Validation;

use Valitron\Validator;

class BaseValidation
{
    protected $rules = [];

    protected $errors = [];

    public function errors()
    {
        return $this->errors;
    }

    public function validate($request)
    {
        $validator = new Validator($request->getParsedBody());

        $validator->mapFieldsRules($this->rules);

        $validate = $validator->validate();

        $this->errors = $validator->errors();

        return $validate;
    }
}
