<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  protected $table = 'users';

  protected $fillable = [
    'name',
    'surname',
    'address',
    'pcode',
    'city',
    'phone',
    'email',
    'country',
    'language'
  ];

  public static function getUser($id)
  {
    $user = self::find($id);

    return [
      'data' => $user->toArray()
    ];
  }

  public function setUser($params)
  {
    $user = $this->create($params);

    return $user;
  }
}
