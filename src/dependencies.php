<?php

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule) {
  return $capsule;
};

$container['view'] = new \Slim\Views\PhpRenderer(__DIR__ . '/templates/');

$container['view']->setLayout("layout.php");

$container['UserController'] = function ($container) {
  return new \App\Controllers\UserController($container);
};

$container['UserValidation'] = function () {
  return new \App\Validation\UserValidation();
};
