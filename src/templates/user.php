<div class="wrapper">
  <form action="./users" method="post">
    <p class="title-form">Wypełnij poniższy formularz</p>
    <p class="info">* Pola wymagane</p>
    <ul>
      <li>
        <label for="name">Imię:*</label>
        <input name="name" id="name" type="text" value="<?php echo isset($params['name']) ? $params['name'] : ''; ?>">
      </li>
      <li>
        <label for="surname">Nazwisko:*</label>
        <input name="surname" id="surname" type="text" value="<?php echo isset($params['surname']) ? $params['surname'] : ''; ?>">
      </li>
      <li>
        <label for="address">Ulica, numer domu i lokalu:*</label>
        <input name="address" id="address" type="text" value="<?php echo isset($params['address']) ? $params['address'] : ''; ?>">
      </li>
      <li>
        <label for="city">Kod pocztowy i miejscowość:*</label>
        <div class="city-wrapper">
          <input name="pcode" id="pcode" type="tel" maxlength="6" value="<?php echo isset($params['pcode']) ? $params['pcode'] : ''; ?>">
          <input name="city" id="city" type="text" value="<?php echo isset($params['city']) ? $params['city'] : ''; ?>">
        </div>
      </li>
      <li>
        <label for="phone">Telefon kontaktowy:*</label>
        <input name="phone" id="phone" type="tel" maxlength="30" value="<?php echo isset($params['phone']) ? $params['phone'] : ''; ?>">
      </li>
      <li>
        <label for="email">Adres e-mail:*</label>
        <input name="email" id="email" type="email" value="<?php echo isset($params['email']) ? $params['email'] : ''; ?>">
      </li>
      <li>
        <label for="country">Kraj:*</label>
        <select name="country" id="country" value="Polska">
          <?php
          $options = array('Afganistan', 'Albania', 'Algieria', 'Australia', 'Bahamy', 'Belgia', 'Białoruś', 'Chiny', 'Chorwacja', 'Dania', 'Francja', 'Grecja', 'Gwinea', 'Hiszpania', 'Irlandia', 'Jamajka', 'Japonia', 'Kanada', 'Korea Południowa', 'Kuba', 'Meksyk', 'Monako', 'Panama', 'Polska', 'Portugalia', 'Rosja', 'Szwecja');

          foreach ($options as $key => $value) { ?>

            <option value="<?php echo $value; ?>" <?php
                                                  echo !isset($params['country']) && $value == 'Polska' ? 'selected="selected"' : '';
                                                  echo isset($params['country']) && $value == $params['country'] ? ' selected="selected"' : ''; ?>><?php echo $value; ?></option>

          <?php } ?>
        </select>
      </li>
      <li>
        <label for="language">Wybierz język obcy jakiego chcesz się nauczyć:*</label>
        <select name="language" id="language">
          <?php
          $options = array('Angielski', 'Chiński', 'Czeski', 'Duński', 'Francuski', 'Grecki', 'Hiszpański', 'Holenderski', 'Norweski', 'Niemiecki', 'Słowacki', 'Szwedzki', 'Portugalski', 'Rosyjski', 'Włoski');

          foreach ($options as $key => $value) { ?>
            <option value="<?php echo $value; ?>" <?php echo isset($params['language']) && $value == $params['language'] ? ' selected="selected"' : ''; ?>><?php echo $value; ?></option>

          <?php } ?>
        </select>
      </li>
    </ul>
    <button type="submit">Zamawiam</button>
  </form>
</div>

<script>
  var errors = <?php echo $errors; ?>;
</script>

<script src="/js/skrypt.js"></script>