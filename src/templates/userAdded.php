<div class="wrapper">
  <div class="user-data">
    <h1>Dziękujemy za dołączenie do grona naszych użytkowników!</h1>
    <h2>Twoje dane:</h2>
    <ul>
      <li>
        <div class="label">Imię:</div>
        <div class="value"><?php echo $user['name']; ?></div>
      </li>
      <li>
        <div class="label">Nazwisko:</div>
        <div class="value"><?php echo $user['surname']; ?></div>
      </li>
      <li>
        <div class="label">Ulica, numer domu i lokalu:</div>
        <div class="value"><?php echo $user['address']; ?></div>
      </li>
      <li>
        <div class="label">Kod pocztowy i miejscowość:</div>
        <div class="value"><?php echo $user['pcode'] . ' ' . $user['city']; ?></div>
      </li>
      <li>
        <div class="label">Telefon kontaktowy:</div>
        <div class="value"><?php echo $user['phone']; ?></div>
      </li>
      <li>
        <div class="label">Adres e-mail:</div>
        <div class="value"><?php echo $user['email']; ?></div>
      </li>
      <li>
        <div class="label">Kraj:</div>
        <div class="value"><?php echo $user['country']; ?></div>
      </li>
      <li>
        <div class="label">Wybrany język obcy:</div>
        <div class="value"><?php echo $user['language']; ?></div>
      </li>
    </ul>
  </div>
</div>